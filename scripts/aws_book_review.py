
from pyspark.sql import SparkSession
from pyspark.sql.functions import col

S3_DATA_SOURCE_PATH = 's3://emrbuckettestsumit/x21154589_input/reviews_Books_5.json.gz'
S3_DATA_OUTPUT_PATH = 's3://emrbuckettestsumit/x21154589_output/aws_book_review'



def main():

    spark    = SparkSession.builder.appName('projectSumit').getOrCreate()
    all_data = spark.read.json(S3_DATA_SOURCE_PATH, header=True)
    print("Total number of records in the source data: %s" % all_data.count())
    # df.where("gender == 'M'")
    products_df = all_data.select(["reviewerID", "reviewerName"]).drop_duplicates()
    print("The number of negineers who work more than 45 hours a week in the US is: %s" % products_df.count())
    products_df.write.mode('overwrite').parquet(S3_DATA_OUTPUT_PATH)
    print("Selected data was successfully saved to s3: %s" % S3_DATA_OUTPUT_PATH)
    
if __name__ == '__main__':
    main()